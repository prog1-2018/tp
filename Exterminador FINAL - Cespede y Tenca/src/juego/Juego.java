package juego;

import java.awt.Color;
import java.awt.Rectangle;
import java.util.Random;
import entorno.Entorno;
import entorno.InterfaceJuego;

public class Juego extends InterfaceJuego {
	// El objeto Entorno que controla el tiempo y otros
	private Entorno entorno;

	// Variables y métodos propios de cada grupo
	private boolean termino;
	private Random generador;
	private Edificio[] edificios;
	private Exterminador ex;
	private Arania[] aranias;
	private Red[] redes;
	private int contadorDeAranias, ticksParaAranias, tiempoParaAranias, arania;
	private String contador, cartelFinal;
	private Bala[] balas;
	private Mina[] minas;
	private Explosion[] explosiones;

	Juego() {
		// Inicializa el objeto entorno
		this.entorno = new Entorno(this, "Exterminador - C�spede y Tenca - Version final", 1024, 768);

		// Inicializar lo que haga falta para el juego
		this.termino = false;
		this.contadorDeAranias = 0;
		this.contador = "" + this.contadorDeAranias;
		this.cartelFinal = "SIMULATION ENDED";
		this.ticksParaAranias = 0;

		// Inicializa el generador de numeros aleatorios
		this.generador = new Random();
		this.tiempoParaAranias = this.generador.nextInt(4 - 1) + 1;

		// Inicializa el exterminador
		this.ex = new Exterminador(this.entorno.ancho() / 2, this.entorno.alto() / 2, 22, 25, 0.0);

		// Inicializacion de los edificios
		int cantidadDeEdificios = this.generador.nextInt(9 - 4) + 4;
		this.edificios = new Edificio[cantidadDeEdificios];
		for (int i = 0; i < cantidadDeEdificios; i++) {
			this.edificios[i] = new Edificio(this.generador.nextInt(this.entorno.ancho()),
					this.generador.nextInt(this.entorno.alto()), this.generador.nextInt(200 - 75) + 75,
					this.generador.nextInt(200 - 75) + 75);
			if (hayColision(this.ex.getBordes(), this.edificios[i].getBordes())) {
				i--;
			}
		}

		// Inicializacion de las primeras arañas
		this.aranias = new Arania[10];
		this.redes = new Red[10];
		for (int i = 0; i < this.aranias.length; i++) {
			this.aranias[i] = crearAraniaNueva();
		}

		// Inicializacion del array de balas
		this.balas = new Bala[0];

		// Inicializacion de minas
		this.minas = new Mina[0];
		this.explosiones = new Explosion[0];

		// Inicia el juego!
		this.entorno.iniciar();
	}

	/**
	 * Durante el juego, el método tick() será ejecutado en cada instante y por lo
	 * tanto es el método más importante de esta clase. Aquí se debe actualizar
	 * el estado interno del juego para simular el paso del tiempo (ver el enunciado
	 * del TP para mayor detalle).
	 */
	public void tick() {
		// Procesamiento de un instante de tiempo

		// COMANDOS DEL TECLADO

		// Rotar a la derecha
		if (this.entorno.estaPresionada(this.entorno.TECLA_DERECHA)) {
			this.ex.rotarDerecha();
		}

		// Rotar a la izquierda
		if (this.entorno.estaPresionada(this.entorno.TECLA_IZQUIERDA)) {
			this.ex.rotarIzquierda();
		}

		// Moverse hacia adelante solo si no se colision con edificios y/o
		// arañas. Si se
		// colisiona con un edificio, el exterminador se mueve un paso hacia
		// atras
		if (this.entorno.estaPresionada(this.entorno.TECLA_ARRIBA)) {
			if (!colisionaConEdificios(this.ex.getBordes()) && !colisionaConAranias(this.ex.getBordes())) {
				this.ex.moverHaciaAdelante();
			}
			if (colisionaConEdificios(this.ex.getBordes())) {
				this.ex.moverHaciaAtras();
			}
			if (colisionaConRedes(this.ex.getBordes())) {
				this.ex.moverHaciaAdelanteMasLento();
			}
		}

		// Disparar solo si el exterminador esta vivo
		if (this.entorno.sePresiono(this.entorno.TECLA_ESPACIO)) {
			if (this.ex.estaVivo()) {
				this.balas = agregarBalaNueva(this.ex.disparar());
			}
		}

		// Soltar minas solo si el exterminador esta vivo
		if (this.entorno.sePresiono('m')) {
			if (this.ex.estaVivo()) {
				this.minas = agregarMinaNueva(this.ex.dejarMina());
				this.explosiones = crearExplosionesNuevas();
			}
		}

		// VERIFICACION DE COLISIONES

		// Colision entre las arañas y el exterminador. Si el exterminador
		// colisiona con una araña viva, este muere y el juego termina
		for (int i = 0; i < this.aranias.length; i++) {
			if (colisionaConAranias(this.ex.getBordes())) {
				this.ex.morir();
				this.termino = true;
			}
		}

		// Colision entre los edificios y las balas. Si colisionan, la bala se
		// frena
		for (int i = 0; i < this.balas.length; i++) {
			if (colisionaConEdificios(this.balas[i].getBordes())) {
				this.balas[i].impactar();
			}
		}

		// Colision entre las balas sin impactar y las arañas vivas. Si
		// colisionan, la araña muere
		for (int i = 0; i < this.aranias.length; i++) {
			for (int j = 0; j < this.balas.length; j++) {
				if (hayColision(this.aranias[i].getBordes(), this.balas[j].getBordes()) && this.aranias[i].estaViva()
						&& !this.balas[j].impacto()) {
					this.aranias[i].morir();
					this.balas[j].impactar();
				}
			}
		}

		// Colision entre las balas sin impactar y las minas sin explotar. Si
		// colisionan, la bala impacta y la mina exlota
		for (int i = 0; i < this.minas.length; i++) {
			for (int j = 0; j < this.balas.length; j++) {
				if (hayColision(this.minas[i].getBordes(), this.balas[j].getBordes()) && !this.minas[i].exploto()
						&& !this.balas[j].impacto()) {
					this.minas[i].explotar();
					this.explosiones[i].activar();
					this.balas[j].impactar();
				}
			}
		}

		// Colision entre las explosiones de las minas y el exterminador, si
		// colisionan, el exterminador muere
		for (int i = 0; i < this.explosiones.length; i++) {
			if (hayColision(this.ex.getBordes(), this.explosiones[i].getBordes()) && this.explosiones[i].estaActiva()
					&& this.ex.estaVivo()) {
				this.ex.morir();
				this.termino = true;
			}
		}

		// Colision entre las minas sin explotar y las arañas vivas. Si
		// colisionan, la mina explota y la araña muere
		for (int i = 0; i < this.aranias.length; i++) {
			for (int j = 0; j < this.minas.length; j++) {
				if (hayColision(this.aranias[i].getBordes(), this.minas[j].getBordes()) && this.aranias[i].estaViva()
						&& !this.minas[j].exploto()) {
					this.aranias[i].morir();
					this.minas[j].explotar();
					this.explosiones[j].activar();
				}
			}
		}

		// Colision entre la explosion de las minas y las arañas vivas
		for (int i = 0; i < this.explosiones.length; i++) {
			for (int j = 0; j < this.aranias.length; j++) {
				if (hayColision(this.explosiones[i].getBordes(), this.aranias[j].getBordes())) {
					if (this.aranias[j].estaViva() && this.explosiones[i].estaActiva()) {
						this.aranias[j].morir();
					}
				}
			}
		}

		// Colision entre las explosiones de las minas y otra mina
		for (int i = 0; i < this.minas.length; i++) {
			for (int j = 0; j < this.explosiones.length; j++) {
				if (i == j) {
					j++;
				} else if (hayColision(this.minas[i].getBordes(), this.explosiones[j].getBordes())
						&& !this.minas[i].exploto() && this.explosiones[j].estaActiva()) {
					this.minas[i].explotar();
					this.explosiones[i].activar();
				}
			}
		}

		// MISC

		// Generador de arañas cada diferente cantidad de tiempo
		this.ticksParaAranias++;
		if (this.tiempoParaAranias == 1) {
			if (this.ticksParaAranias == 32 && !this.termino) {
				this.aranias = agregarAraniaNueva();
				this.redes = agregarRedNueva();
				this.tiempoParaAranias = this.generador.nextInt(4 - 1) + 1;
				this.ticksParaAranias = 0;
			}
		}
		if (this.tiempoParaAranias == 2) {
			if (this.ticksParaAranias == 64 && !this.termino) {
				this.aranias = agregarAraniaNueva();
				this.redes = agregarRedNueva();
				this.tiempoParaAranias = this.generador.nextInt(4 - 1) + 1;
				this.ticksParaAranias = 0;
			}
		}
		if (this.tiempoParaAranias == 3) {
			if (this.ticksParaAranias == 96 && !this.termino) {
				this.aranias = agregarAraniaNueva();
				this.redes = agregarRedNueva();
				this.tiempoParaAranias = this.generador.nextInt(4 - 1) + 1;
				this.ticksParaAranias = 0;
			}
		}

		// Contar arañas muertas
		this.contadorDeAranias = contarAraniasMuertas();
		this.contador = "" + this.contadorDeAranias;

		// Soltar redes de arañas cada cierto tiempo
		this.arania = this.generador.nextInt(this.aranias.length - 1);
		this.aranias[this.arania].contarTick();
		if (this.redes[this.arania] == null && !this.termino) {
			this.redes[this.arania] = this.aranias[this.arania].soltarRed();
		}

		// DIBUJADO

		// Dibujado de edificios
		if (!this.termino) {
			for (int i = 0; i < this.edificios.length; i++) {
				this.edificios[i].dibujar(this.entorno);
			}
		}

		// Dibujado de redes durante 750 ticks
		if (!this.termino) {
			for (int i = 0; i < this.redes.length; i++) {
				if (this.redes[i] != null) {
					if (this.redes[i].estaActiva()) {
						this.redes[i].dibujar(this.entorno);
						this.redes[i].contarTick();
						this.redes[i].desactivar();
					}
				}
			}
		}

		// Dibujado y movimiento de arañas vivas
		if (!this.termino) {
			for (int i = 0; i < this.aranias.length; i++) {
				if (this.aranias[i].estaViva()) {
					this.aranias[i].dibujar(this.entorno);
					if (this.ex.estaVivo()) {
						this.aranias[i].perseguir(this.ex, this.edificios);
					}
				}
			}
		}

		// Dibujado de minas
		if (!this.termino) {
			for (int i = 0; i < this.minas.length; i++) {
				if (!this.minas[i].exploto()) {
					this.minas[i].dibujar(this.entorno);
				}
			}
		}

		// Dibujado de explosiones
		if (!this.termino) {
			for (int i = 0; i < this.explosiones.length; i++) {
				if (this.explosiones[i].estaActiva()) {
					this.explosiones[i].dibujar(this.entorno);
					this.explosiones[i].contarTick();
					this.explosiones[i].desactivar();
				}
			}
		}

		// Dibujado de balas
		if (!this.termino) {
			for (int i = 0; i < this.balas.length; i++) {
				if (!this.balas[i].impacto()) {
					this.balas[i].dispararse();
					this.balas[i].dibujar(this.entorno);
				}
			}
		}

		// Dibujado de exterminador solo si esta vivo
		if (!this.termino) {
			this.ex.dibujar(this.entorno);
		}

		// Dibujado de contador
		this.entorno.cambiarFont(this.contador, 50, Color.RED);
		this.entorno.escribirTexto(this.contador, this.entorno.ancho() - 100, 50);

		// Dibujado de GAME OVER
		if (this.termino) {
			this.entorno.cambiarFont(this.cartelFinal, 75, Color.RED);
			this.entorno.escribirTexto(this.cartelFinal, this.entorno.ancho() - (this.entorno.ancho() * 0.85),
					this.entorno.alto() / 2);
		}

	}

	// FUNCIONES AUXILIARES

	// Detector de colisiones
	private boolean hayColision(Rectangle r1, Rectangle r2) {
		if (r1.intersects(r2)) {
			return true;
		} else {
			return false;
		}
	}

	// Detector de colisiones con todas las arañas VIVAS
	private boolean colisionaConAranias(Rectangle r1) {
		for (int i = 0; i < this.aranias.length; i++) {
			if (hayColision(r1, this.aranias[i].getBordes()) && this.aranias[i].estaViva()) {
				return true;
			}
		}
		return false;
	}

	// Detector de colisiones con todos los edificios
	private boolean colisionaConEdificios(Rectangle r1) {
		for (int i = 0; i < this.edificios.length; i++) {
			if (hayColision(r1, this.edificios[i].getBordes())) {
				return true;
			}
		}
		return false;
	}

	// Detector de colisiones con todas las redes
	private boolean colisionaConRedes(Rectangle r1) {
		for (int i = 0; i < this.redes.length; i++) {
			if (this.redes[i] != null && hayColision(r1, this.redes[i].getBordes()) && this.redes[i].estaActiva()) {
				return true;
			}
		}
		return false;
	}

	// Creador de array de arañas
	private Arania[] agregarAraniaNueva() {
		Arania[] arNuevo = new Arania[this.aranias.length + 1];
		for (int i = 0; i < this.aranias.length; i++) {
			arNuevo[i] = this.aranias[i];
		}
		arNuevo[arNuevo.length - 1] = crearAraniaNueva();
		return arNuevo;
	}

	private Red[] agregarRedNueva() {
		Red[] redesNuevo = new Red[this.redes.length + 1];
		for (int i = 0; i < this.redes.length; i++) {
			redesNuevo[i] = this.redes[i];
		}
		return redesNuevo;
	}

	// Creador de arañas
	private Arania crearAraniaNueva() {
		int borde = this.generador.nextInt(4);
		Arania arania = null;
		if (borde == 0) {
			int coordenadaAleatoria = this.generador.nextInt(769);
			arania = new Arania(coordenadaAleatoria, 0, 15, 15);
		}
		if (borde == 1) {
			int coordenadaAleatoria = this.generador.nextInt(1025);
			arania = new Arania(this.entorno.ancho(), coordenadaAleatoria, 15, 15);
		}
		if (borde == 2) {
			int coordenadaAleatoria = this.generador.nextInt(769);
			arania = new Arania(coordenadaAleatoria, this.entorno.alto(), 15, 15);
		}
		if (borde == 3) {
			int coordenadaAleatoria = this.generador.nextInt(1025);
			arania = new Arania(0, coordenadaAleatoria, 15, 15);
		}
		return arania;
	}

	// Creador de array de balas
	private Bala[] agregarBalaNueva(Bala bala) {
		Bala[] balasNuevo = new Bala[this.balas.length + 1];
		for (int i = 0; i < this.balas.length; i++) {
			balasNuevo[i] = this.balas[i];
		}
		balasNuevo[balasNuevo.length - 1] = bala;
		return balasNuevo;
	}

	// Contador de arañas muertas
	private int contarAraniasMuertas() {
		int suma = 0;
		for (int i = 0; i < this.aranias.length; i++) {
			if (!this.aranias[i].estaViva()) {
				suma++;
			}
		}
		return suma;
	}

	// Creador de array de minas
	private Mina[] agregarMinaNueva(Mina mina) {
		Mina[] minasNuevo = new Mina[this.minas.length + 1];
		for (int i = 0; i < this.minas.length; i++) {
			minasNuevo[i] = this.minas[i];
		}
		minasNuevo[minasNuevo.length - 1] = mina;
		return minasNuevo;
	}

	// Creador de array de explosiones
	private Explosion[] crearExplosionesNuevas() {
		Explosion[] explosionesNuevo = new Explosion[this.explosiones.length + 1];
		for (int i = 0; i < this.explosiones.length; i++) {
			explosionesNuevo[i] = this.explosiones[i];
		}
		explosionesNuevo[explosionesNuevo.length - 1] = new Explosion(this.ex.getX(), this.ex.getY(), 50);
		return explosionesNuevo;
	}

	@SuppressWarnings("unused")
	public static void main(String[] args) {
		Juego juego = new Juego();
	}
}
