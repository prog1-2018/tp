package juego;

import java.awt.Color;
import java.awt.Rectangle;
import entorno.Entorno;

public class Exterminador {

	private double x, y, direccion;
	private int alto, ancho;
	private boolean vida;

	public Exterminador(double x, double y, int alto, int ancho, double direccion) {
		this.x = x;
		this.y = y;
		this.alto = alto;
		this.ancho = ancho;
		this.direccion = direccion;
		this.vida = true;
	}

	public void dibujar(Entorno e) {
		e.dibujarTriangulo(this.x, this.y, this.alto, this.ancho, this.direccion, Color.BLUE);
	}

	public void moverHaciaAdelante() {
		this.x += Math.cos(this.direccion) * 2;
		this.y += Math.sin(this.direccion) * 2;
	}

	public void moverHaciaAdelanteMasLento() {
		this.x += (Math.cos(this.direccion) * 2) * -0.6;
		this.y += (Math.sin(this.direccion) * 2) * -0.6;
	}

	public void moverHaciaAtras() {
		this.x += -(Math.cos(this.direccion) * 2);
		this.y += -(Math.sin(this.direccion) * 2);
	}

	public void rotarDerecha() {
		this.direccion += 0.03;
	}

	public void rotarIzquierda() {
		this.direccion -= 0.03;
	}

	public boolean estaVivo() {
		return this.vida;
	}

	public void morir() {
		this.vida = false;
	}

	public Bala disparar() {
		return new Bala(this.x, this.y, 3, 10, this.direccion);
	}

	public Mina dejarMina() {
		return new Mina(this.x, this.y, 10);
	}

	public double getX() {
		return this.x;
	}

	public double getY() {
		return this.y;
	}

	public Rectangle getBordes() {
		return new Rectangle((int) (Math.round(this.x) - (this.ancho / 2) + 2),
				(int) (Math.round(this.y) - (this.alto / 2) + 2), this.ancho - 4, this.alto - 4);
	}

}
