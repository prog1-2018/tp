package juego;

import java.awt.Color;
import java.awt.Rectangle;
import entorno.Entorno;

public class Edificio {

	private double x, y;
	private int alto, ancho;

	public Edificio(double x, double y, int alto, int ancho) {
		this.x = x;
		this.y = y;
		this.alto = alto;
		this.ancho = ancho;
	}

	public void dibujar(Entorno e) {
		e.dibujarRectangulo(this.x, this.y, this.ancho, this.alto, 0.0, Color.GRAY);
	}

	public Rectangle getBordes() {
		return new Rectangle((int) (Math.round(this.x) - (this.ancho / 2)),
				(int) (Math.round(this.y) - (this.alto / 2)), this.ancho, this.alto);
	}
}