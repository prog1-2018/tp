package juego;

import java.awt.Color;
import java.awt.Rectangle;
import entorno.Entorno;

public class Mina {

	private double x, y;
	private int diametro;
	private boolean explosion;

	public Mina(double x, double y, int diametro) {
		this.x = x;
		this.y = y;
		this.diametro = diametro;
		this.explosion = false;
	}

	public void dibujar(Entorno e) {
		e.dibujarCirculo(this.x, this.y, this.diametro, Color.RED);
	}

	public void explotar() {
		this.explosion = true;
	}

	public boolean exploto() {
		return this.explosion;
	}

	public Rectangle getBordes() {
		return new Rectangle((int) (Math.round(this.x) - (this.diametro / 2)),
				(int) (Math.round(this.y) - (this.diametro / 2)), this.diametro, this.diametro);
	}

}