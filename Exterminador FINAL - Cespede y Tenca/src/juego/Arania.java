package juego;

import java.awt.Color;
import java.awt.Rectangle;
import java.util.Random;
import entorno.Entorno;

public class Arania {

	private double x, y;
	private int alto, ancho, ticks, tiempo;
	private boolean vida, solto;
	private Random generador;

	public Arania(double x, double y, int alto, int ancho) {
		this.x = x;
		this.y = y;
		this.alto = alto;
		this.ancho = ancho;
		this.vida = true;
		this.solto = false;
		this.generador = new Random();
		this.ticks = 0;
		this.tiempo = this.generador.nextInt(100);
	}

	public void dibujar(Entorno e) {
		e.dibujarRectangulo(this.x, this.y, this.ancho, this.alto, 0.0, Color.WHITE);
		;
	}

	public void perseguir(Exterminador ex, Edificio[] ed) {
		if (ex.getX() > this.x) {
			this.x += 0.25;
			if (colisionaConEdificios(this.getBordes(), ed)) {
				this.retroceder(ex);
				if (ex.getY() > this.y) {
					this.y += 0.25;
				} else {
					this.y -= 0.25;
				}
			}
		} else if (ex.getX() < this.x) {
			this.x -= 0.25;
			if (colisionaConEdificios(this.getBordes(), ed)) {
				this.retroceder(ex);
				if (ex.getY() < this.y) {
					this.y -= 0.25;
				} else {
					this.y += 0.25;
				}

			}
		} else {
			if (ex.getY() < this.y) {
				this.y -= 0.25;
			} else {
				this.y += 0.25;
			}
		}
		if (ex.getY() > this.y) {
			this.y += 0.25;
			if (colisionaConEdificios(this.getBordes(), ed)) {
				this.retroceder(ex);
				if (ex.getX() < this.x) {
					this.x -= 0.25;
				} else {
					this.x += 0.25;
				}

			}
		} else if (ex.getY() < this.y) {
			this.y -= 0.25;
			if (colisionaConEdificios(this.getBordes(), ed)) {
				this.retroceder(ex);
				if (ex.getX() > this.x) {
					this.x += 0.25;
				} else {
					this.x -= 0.25;
				}
			}
		} else {
			if (ex.getX() > this.x) {
				this.x += 0.25;
			} else {
				this.x -= 0.25;
			}
		}
	}

	public boolean estaViva() {
		return this.vida;
	}

	public void morir() {
		this.vida = false;
	}

	public Red soltarRed() {
		if (this.ticks == this.tiempo) {
			if (this.estaViva() && !this.soltoRed()) {
				this.solto = true;
				return new Red(this.x, this.y, 40);
			}
		}
		return null;
	}

	public boolean soltoRed() {
		return this.solto;
	}

	public void contarTick() {
		this.ticks++;
	}

	public Rectangle getBordes() {
		return new Rectangle((int) (Math.round(this.x) - (this.ancho / 2)),
				(int) (Math.round(this.y) - (this.alto / 2)), this.ancho, this.alto);
	}

	// Detector de colisiones
	private boolean hayColision(Rectangle r1, Rectangle r2) {
		if (r1.intersects(r2)) {
			return true;
		} else {
			return false;
		}
	}

	// Detector de colisiones con todos los edificios
	private boolean colisionaConEdificios(Rectangle r1, Edificio[] ed) {
		for (int i = 0; i < ed.length; i++) {
			if (hayColision(r1, ed[i].getBordes())) {
				return true;
			}
		}
		return false;
	}

	// Dar un paso atras
	private void retroceder(Exterminador ex) {
		if (ex.getX() > this.x) {
			this.x += -0.25;
		} else if (ex.getX() < this.x) {
			this.x -= -0.25;
		}
		if (ex.getY() > this.y) {
			this.y += -0.25;
		} else if (ex.getY() < this.y) {
			this.y -= -0.25;
		}
	}
}