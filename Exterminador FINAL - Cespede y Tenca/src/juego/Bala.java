package juego;

import java.awt.Color;
import java.awt.Rectangle;
import entorno.Entorno;

public class Bala {

	private double x, y, angulo;
	private int alto, ancho;
	private boolean impacto;

	public Bala(double x, double y, int alto, int ancho, double angulo) {
		this.x = x;
		this.y = y;
		this.alto = alto;
		this.ancho = ancho;
		this.angulo = angulo;
		this.impacto = false;
	}

	public void dibujar(Entorno e) {
		e.dibujarRectangulo(this.x, this.y, this.ancho, this.alto, this.angulo, Color.GREEN);
	}

	public void dispararse() {
		this.x += Math.cos(this.angulo) * 3;
		this.y += Math.sin(this.angulo) * 3;
	}

	public void impactar() {
		this.impacto = true;
	}

	public boolean impacto() {
		return impacto;
	}

	public Rectangle getBordes() {
		return new Rectangle((int) (Math.round(this.x) - (this.ancho / 2)),
				(int) (Math.round(this.y) - (this.alto / 2)), this.ancho, this.alto);
	}
}
