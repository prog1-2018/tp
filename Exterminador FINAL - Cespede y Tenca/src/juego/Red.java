package juego;

import java.awt.Color;
import java.awt.Rectangle;
import entorno.Entorno;

public class Red {
	private double x, y;
	private int diametro, ticks;
	private boolean activa;

	public Red(double x, double y, int diametro) {
		this.x = x;
		this.y = y;
		this.diametro = diametro;
		this.activa = true;
		this.ticks = 750;
	}

	public void dibujar(Entorno e) {
		e.dibujarCirculo(this.x, this.y, this.diametro, Color.GRAY);
	}

	public boolean estaActiva() {
		return this.activa;
	}

	public void desactivar() {
		if (this.ticks == 0) {
			this.activa = false;
		}
	}

	public void contarTick() {
		this.ticks--;
	}

	public Rectangle getBordes() {
		return new Rectangle((int) (Math.round(this.x) - (this.diametro / 2)),
				(int) (Math.round(this.y) - (this.diametro / 2)), this.diametro, this.diametro);
	}
}
